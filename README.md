# Desafio Arquitetura	
### Concepção da arquitetura:

A princípio utilizaria postgres (AWS RDS) na Base A, configurado para ser acessível apenas pela rede interna, com criptografia em cada item dos dados. Caso necessário uma busca pela base seria simples criptografar o dado antes de fazer uma busca no banco. Incluiria um UUID para outros serviços poderem associar o item em sua base.
Para fazer uma requisição à primeira api que acessa os dados da Base A, implementaria o OAuth 2.0 para fazer validação dos consumidores.

Quanto a Base B, utilizaria mongoDB para gravar documentos referentes a cada cliente, dando uma leitura mais rápida para a aplicação que for consumi-lo. Incluiria o UUID referente ao usuário da BaseA. Continuaria com a autenticação via OAuth 2.0.

Para o último serviço, aplicaria Event Sourcing e CQRS, assim daria agilidade nas inserções e com a camada de leitura seria simples fazer o rastreio. Como agregado utilizaria o CPF e suas principais entidades seriam o Bureau de Crédito, Movimentações financeiras e Compras com cartão, teria também o objeto de valor UUID da Base A. Como base utilizaria MongoDB para o Event Store, e o Kafka como event bus. Na prática, as ações de consulta, movimentação financeira e compra com cartão, produzem comandos para api que traduz em eventos que, por sua vez, atualizam o agregado. Após inserção deste evento no Event Store, seria disparado o novo evento ao Kafka que por sua vez aciona a camada de leitura que é atualizada podendo, por exemplo, ser persistida no S3 (AWS) ou uma base MongoDB diferente, dividindo assim a camada de escrita da de leitura. Ideal também termos um cache dessas chamadas, possivelmente no redis.


### Tecnologias adjacentes:

  - Para garantir a escalabilidade dos serviços, todos seriam “conteinerizados” com Docker e estocados no ECS.
  - Salvariam seus logs no Redis para logo depois serem inseridos no Elasticsearch para podermos fazer um primeiro nível de monitoramento.


### Para garantir o deploy das aplicações de forma segura:

O processo de deploy utilizaria uma trigger no bitbucket baseada no pull request para o master, onde dispararia o comando do Jenkins p/ orquestrar os testes:

  - Clonar o projeto que queira fazer o deploy;
  - Mergiar o Pull request no branch Master apenas localmente;
  - Levantar um container com o master atualizado;
  - Clonar os projetos dependentes e levantar seus containers;
  - Rodar os testes unitários e integração da aplicação;
  - Rodar testes de contrato com as outras apis;
  - Caso os testes estejam corretos:
    - Atualizar as imagens no ECS;
    - Utilizar o CodeDeploy para subir as novas imagens em produção;
    - Caso o Deploy ocorra corretamente, aprovar e mergiar o pull request no Bitbucket;
    - Caso o Deploy falhe volta as imagens do ECS com a ultima release e notifica;
  - Caso falha disparar alerta e/ou notificação. Atualmente utilizamos o Slack, mas é possível notificar via telegram por exemplo.
