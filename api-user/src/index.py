from flask import Flask
from flask_restful import Api
from src.app.provider.controller_provider import Controller_Provider

app = Flask(__name__)
api = Api(app)

Controller_Provider(api)

if __name__ == "__main__":
    app.run(port='5002')
