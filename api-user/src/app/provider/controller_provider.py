from flask_restful import Api
from src.app.controller.employee_controller import Employee_Controller, Employee_Name_Controller


class Controller_Provider:
    def __init__(self, api: Api):
        api.add_resource(Employee_Controller, '/employees')
        api.add_resource(Employee_Name_Controller, '/employees/<employee_id>')
