from mysql import connector
import os


def mysql_connection():
    return connector.connect(
        host=os.environ.get_list('MYSQL_HOST'),
        user=os.environ.get_list('MYSQL_USER_MAXPAY'),
        passwd=os.environ.get_list('MYSQL_PASSWORD_MAXPAY'),
        database=os.environ.get_list('MYSQL_DBNAME_MAXPAY')
    )
