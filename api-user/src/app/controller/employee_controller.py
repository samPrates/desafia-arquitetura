from src.domain.employee import Employees
from flask_restful import Resource


class Employee_Controller(Resource):
    def get(self):
        return Employees.get_list()


class Employee_Name_Controller(Resource):
    def get(self, employee_id):
        return Employees.get_by_name(employee_id)
