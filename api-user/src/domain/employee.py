from flask_restful import Resource
from flask import jsonify
from src.app.connection.mysql_connection import mysql_connection


class Employees(Resource):
    def __init__(self):
        self._conn = mysql_connection()

    def get_list(self):
        query = self._conn.execute("select * from employees")
        return {'employees': [i[0] for i in query.cursor.fetchall()]}

    def get_by_name(self, employee_id):
        query = self._conn.execute("select * from employees where EmployeeId =%d " % int(employee_id))
        result = {'data': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}
        return jsonify(result)
